import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Provider, SocialLoginService } from 'ng8-social-login';
import { Users } from 'src/app/models/users';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  erroutput : any;
  loginRespone : any;
  submitted = false;
  loginForm: FormGroup;
  loading = false;
  public authorized: boolean = false;
  isShow : boolean;

  constructor(private formBuilder : FormBuilder, private apiservice : ApiservicesService, private router : Router,
   private socialService:  SocialLoginService) {
  }

ngOnInit() {
  localStorage.clear();
  this.loginForm  =  this.formBuilder.group({
    userName: ['', Validators.required],
    password: ['', Validators.required]
  });
}


public socialSignIn(socialProvider: string) {  
  this.loading = true;
   if(socialProvider == 'facebook'){
    this.socialService.login(Provider.FACEBOOK).subscribe(userData =>    {
      if (userData != null) {
        this.authorized = true;
        this.user = new Users();
        this.user = userData; 
        console.log(this.user)
        localStorage.setItem('user',JSON.stringify(this.user))
      this.loading = false; 

        this.router.navigate(['/dashboard']); 
     }    
      console.log(userData);
    });
    // socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  } else if(socialProvider == 'google'){

    this.socialService.login(Provider.GOOGLE).subscribe(userData =>    {
      if (userData != null) {
        this.authorized = true;
        this.user = new Users();
        this.user = userData; 
        console.log(this.user)
        localStorage.setItem('user',JSON.stringify(this.user ))    
        this.router.navigate(['/dashboard']); 
     }     
      console.log(userData);
    });

  }
 
}
 
  get f() { return this.loginForm.controls;}
  user : any;
   name : string;
login(){
  this.submitted = true;
  if (this.loginForm.invalid) {
    return;
  }
  this.user = new Users();
  this.user.userName = this.loginForm.value.userName;
  this.user.password = this.loginForm.value.password;
  this.apiservice.loginUser(this.user).subscribe(res => {
  this.loginRespone = res;
  console.log(this.loginRespone);
  if(this.loginRespone.status == 200){
    localStorage.setItem('user',null);
   this.name= this.loginRespone.user.firstName + ' ' + this.loginRespone.user.lastName;
  
    localStorage.setItem('name',this.name)
    this.loading =  false;
    
    if(this.loginRespone.user.role.roleName == 'Academic Admin'){
     
      this.router.navigate(['dashboard', {login : this.loginRespone}]);
    }else if(this.loginRespone.user.role.roleName =='Paid Student'){
      localStorage.setItem('userId',this.loginRespone.user.userId)
      this.router.navigate(['/myCourses' ]);
    }else if(this.loginRespone.user.role.roleName =='FREE'){
        this.router.navigate(['/otherCourses' ]);
      }
    this.loginForm.reset();
    
  }else{
    this.erroutput = this.loginRespone.message
    this.submitted = false;
    this.loginForm.reset();
  }
},error=>{
  swal.fire({
    icon: 'warning',
    text: "Some thing went wrong, Please Try Later" ,
  })
});

this.submitted = false;
}



}