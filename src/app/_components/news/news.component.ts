import { Component, OnInit } from '@angular/core';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  constructor(private apiService : ApiservicesService) { }

  newsResponse : any;
  allNews : any;
  deleteNewsRes : any;

  ngOnInit() {
   this.viewNews();
  }
  
viewNews(){
  this.apiService.viewNews().subscribe( res => {
   this.newsResponse = res;
  if(this.newsResponse.status == 200){
    this.allNews = this.newsResponse.news;
    console.log(this.allNews)
  }else{
  Swal.fire({
    icon : "warning",
    text : this.newsResponse.message
  })
  }
  })
}

myFunction(id) {
  var dots = document.getElementById("dots"+id);
  var moreText = document.getElementById("more"+id);
  var dot = document.getElementById("dot"+id);
  var moreTexts = document.getElementById("mores"+id);
  var btnText = document.getElementById("myBtn"+id);

  if (dots.style.display === "none" || dot.style.display === "none") {
    dots.style.display = "inline";
    dot.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
    moreTexts.style.display = "none";
  } else {
    dots.style.display = "none";
    dot.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
    moreTexts.style.display = "inline";
  }
}

  deleteNews(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#D3D3D3',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.deleteNews(id).subscribe(res => {
          this.deleteNewsRes = res; 
        const Toast = Swal.mixin({
          toast: true,
          position: 'top',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
  
        Toast.fire({
          icon: 'success',
          title: this.deleteNewsRes.message,
        })
        this.viewNews();
      })
    }
  }) 
  
  }

}
