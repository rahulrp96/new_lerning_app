import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.scss']
})
export class EditNewsComponent implements OnInit {
 
  editNewsForm : FormGroup;
  submitted = false;
  response : any;
  newsFile : any;
  newsId : any;
  dataById :any[];
  files = "";


  constructor(private formBuilder : FormBuilder, private router : Router,
    private apiservice : ApiservicesService, private route: ActivatedRoute) { }
  
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.newsId = params.get('newsId');
      console.log(this.newsId);
    });

    this.editNewsForm = this.formBuilder.group({
      newsTitle : [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      newsDescription: ['', Validators.required]
   });
   this.getNewsById()
  }

  get f() { return this.editNewsForm.controls; }

  getNewsById(){
    this.apiservice.getNewsById(this.newsId).subscribe(res => {
      this.response = res;
      this.dataById = this.response.newsData;
      console.log(this.dataById)
      })
  }

  selectfile(event){
    if(event.target.files.length > 0){
      for (var i = 0; i < event.target.files.length; i++) {
      const file =event.target.files[i]
      this.files =file;
      }
      console.log(this.files)
    }
  }
  
  editNews(){
    this.submitted = true;
    if (this.editNewsForm.invalid) {
      return;
    }
    const formDatas = new FormData();
    formDatas.append('newsId', this.newsId);
    alert(this.editNewsForm.value.newsTitle)
    formDatas.append('newsTitle',this.editNewsForm.value.newsTitle);
    formDatas.append('newsText',this.editNewsForm.value.newsDescription);
    if(this.files == ""){
      formDatas.append('file',this.files);
    }
    else{
    formDatas.append('file',this.files);
    } 
    this.apiservice.editNews(this.newsId,formDatas).subscribe(res => {
      this.response = res;
    if(this.response.status == 200){
      Swal.fire({
        icon : "success",
        text : this.response.message
      })
    }else{
      Swal.fire({
        icon : "warning",
        text : this.response.message
      })
    }
    this.editNewsForm.reset();
      this.submitted = false;
      this.router.navigate(['/news']);
    }, error =>{
      Swal.fire({
        icon : "warning",
        text : "Some thing went wrong"
      })
    }
    )
  }
}
