import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.scss']
})
export class AddNewsComponent implements OnInit {

  addNewsForm : FormGroup;
  submitted = false;
  result:any;
  files:any;
  
   constructor(private router : Router,private formBuilder : FormBuilder, private apiservice : ApiservicesService) { }

  ngOnInit() {
    this.addNewsForm = this.formBuilder.group({
      newsTitle : [null,Validators.compose([Validators.required, Validators.minLength(2)])],
      newsDescription: ['', Validators.required]
   });
  }
  get f() { return this.addNewsForm.controls; }

  selectfile(event){
    if(event.target.files.length > 0){
      for (var i = 0; i < event.target.files.length; i++) {
      const file =event.target.files[i]
      this.files =file;
      }
      console.log(this.files)
    }
  }

  postaddnews(){
  this.submitted = true;
    if (this.addNewsForm.invalid) {
      return;
    }
    const formDatas = new FormData();
    formDatas.append('newsTitle',this.addNewsForm.value.newsTitle);
    formDatas.append('newsText',this.addNewsForm.value.newsDescription);
    formDatas.append('file',this.files); 
     this.apiservice.addNews(formDatas).subscribe((res)=> {
      this.result =res;
       if (this.result.status==200){
        swal.fire({
          icon: 'success',
          text: this.result.message,
        })   
       }
       else{
        swal.fire({
          icon: 'warning',
          text: this.result.message,
        })
      }
      this.addNewsForm.reset();
      this.submitted = false;
      this.router.navigate(['/news']);
    },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong",
      })
    }
    )
}

}
