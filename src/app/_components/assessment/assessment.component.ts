import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiservicesService } from 'src/app/_services/apiservices.service';

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent implements OnInit {
  panelOpenState = false;
  tablerow=false;
  assessmentId : number;
  response : any;
  responseData : any= [];
  questionDtos : any=[];
  ansers = false;
  constructor(private route : ActivatedRoute, private apiService : ApiservicesService) { }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.getAssessment(res.assessmentId)
     this.assessmentId = res.assessmentId;
    })
  
  }

  getAssessment(assessmentId){
    this.apiService.getAssessmentById(assessmentId).subscribe(res => {
        this.response = res;
        if(this.response.status == 200){
          this.responseData = this.response.data;
          this.questionDtos = this.response.data.questionDtos;
          console.log(this.responseData)
          console.log(this.questionDtos);
        }
    });
  }

  // opnanswer(){
  //   this.ansers = !this.ansers;
  // }

  // lectures(){
  //   var acc = document.getElementsByClassName("accordion");
  //   var i;
  //   for (i = 0; i < acc.length; i++) {
  //     acc[i].addEventListener("click", function() {
  //       this.classList.toggle("active");
  //       var panel = this.nextElementSibling;
  //       if (panel.style.maxHeight) {
  //         panel.style.maxHeight = null;
  //       } else {
  //         panel.style.maxHeight = panel.scrollHeight + "px";
  //       } 
  //     });
  //   }
  //}

}
