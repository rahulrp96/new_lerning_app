import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BlurEvent, ChangeEvent, CKEditor5 } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent implements OnInit {

  assessmentForm : any;
  submitted = false;
  constructor(private formBuilder : FormBuilder) { }
  templateDrivenForm : any;
  public Editor = ClassicEditor;

  public isDisabled = false;

  public componentEvents: string[] = [];

  public form: FormGroup;

  ngOnInit() {

    this.assessmentForm = this.formBuilder.group({
      assessmentName: [null,Validators.compose([Validators.required,Validators.minLength(3)])],
      lastDateOfSubmission: [null,Validators.required],
      uploadAssessment:['',Validators.required],
      description :['']
  });
  }

  get f() { return this.assessmentForm.controls; }
 
  toggleDisableEditors() {
	  this.isDisabled = !this.isDisabled;
  }

  onReady( editor: CKEditor5.Editor ): void {
	  this.componentEvents.push( 'The editor is ready.' );
  }

  onChange( event: ChangeEvent ): void {
	  this.componentEvents.push( 'Editor model changed.' );
  }

  onFocus( event: FocusEvent ): void {
	  this.componentEvents.push( 'Focused the editing view.' );
  }

  onBlur( event: BlurEvent ): void {
	  this.componentEvents.push( 'Blurred the editing view.' );
  }
  
}


