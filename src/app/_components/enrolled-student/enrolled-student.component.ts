import { Component, OnInit } from '@angular/core';
import { ApiservicesService } from '../../_services/apiservices.service';
import swal from 'sweetalert2';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  Name: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { Name: 'ABC'},
  { Name: 'ABC'},
  { Name: 'ABC'},
  
];

@Component({
  selector: 'app-enrolled-student',
  templateUrl: './enrolled-student.component.html',
  styleUrls: ['./enrolled-student.component.scss']
})
export class EnrolledStudentComponent implements OnInit {

  isLoading = true
  assignList:any;
  assignm:any;

    displayedColumns: string[] = ['Name', 'Date', 'Courses','Topics','Upload','Action'];
    dataSource = ELEMENT_DATA;

  constructor(public service : ApiservicesService) { }

  ngOnInit() {
    this.assess();
  }

  assess(){
    document.getElementById('content1-active').style.display='block'
    document.getElementById('indicator').style.display='block'
    document.getElementById('content2-active').style.display='none'
    document.getElementById('indicator1').style.display='none'
  }

  assign(){
    this.getfetchAssignment();
    document.getElementById('indicator1').style.display='block'
    document.getElementById('content2-active').style.display='block'
    document.getElementById('indicator').style.display='none'
    document.getElementById('content1-active').style.display='none'
  }

  getfetchAssignment(){
    this.service.getfetchAssignment().subscribe(res => { 
      this.assignm = res;
      this.assignList = new MatTableDataSource();
      this.isLoading = true;
      this.assignList.data= this.assignm.response.dataList;
      console.log(this.assignList)
     },error=>{
      this.isLoading = false
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong",
      })
    })
}

}
