import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BlurEvent, ChangeEvent, CKEditor5 } from '@ckeditor/ckeditor5-angular';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.scss']
  
})
export class AssignmentComponent implements OnInit {
  submitted = false;
  templateDrivenForm : any;
  public Editor = ClassicEditor;
  uploadAssignmentForm : FormGroup;
  public isDisabled = false;
  public componentEvents: string[] = [];
  public form: FormGroup;
  couseDetails : any;
  courseDataList : any;
  courseId : number; 
  uploadResponse : any;
  contFile:any;
  topicData : any;
  topicDataList : [];
  topicId : any;
  meadifile:any;

  assigmentData = 
    {
      "assignmentDate": "0",
      "assignmentName": "",
      "assignmentsDesc": "",
      "courseId": 0,
      "createdByFaculty": 0,
      "extendedSubmissionDate": "",
      "id": 0,
      "isDeleted": "string",
      "isSubmissionExtended": "string",
      "media" : null,
      "scheduledSubmissionDate": "string",
      "topicId": 0
    }

  constructor(private formBuilder : FormBuilder, private apiService : ApiservicesService,
    private router : Router, public datepipe: DatePipe) { }
  

  ngOnInit() {
    this.uploadAssignmentForm = this.formBuilder.group({
      assignmentName: [null,Validators.compose([Validators.required,Validators.minLength(3)])],
      assignmentDate: [null,Validators.required],
      assignmentsFile : [''],
      descrip : ['']
    });
    this.getmasterCourse();
  }

  get f() { return this.uploadAssignmentForm.controls;}

  toggleDisableEditors() {
	  this.isDisabled = !this.isDisabled;
  }

  onReady( editor: CKEditor5.Editor ): void {
	  this.componentEvents.push( 'The editor is ready.' );
  }

  onChange( event: ChangeEvent ): void {
	  this.componentEvents.push( 'Editor model changed.' );
  }

  onFocus( event: FocusEvent ): void {
	  this.componentEvents.push( 'Focused the editing view.' );
  }

  onBlur( event: BlurEvent ): void {
	  this.componentEvents.push( 'Blurred the editing view.' );
  }
  
  getmasterCourse(){
    this.apiService.getmasterCourse().subscribe(res => {
     this.couseDetails = res;
     this.courseDataList = this.couseDetails.dataList;
     console.log(this.courseDataList)
  })
}


  getcourseId(id){
    this.courseId = id;
    //console.log(this.courseId)
    this.getmastertopics();
  }


  getmastertopics(){
    this.apiService.getmastertopics(this.courseId).subscribe(res => {
     this.topicData = res;
     this.topicDataList = this.topicData.dataList;
    })
  }

  gettopicId(id){
    this.topicId = id;
   }

  selectFile(event){
    if(event.target.files.length > 0){
      for (var i = 0; i < event.target.files.length; i++) {
      const file =event.target.files[i]
      this.contFile = JSON.stringify(file);
      } 
    }
  }

  uploadAssignment(){
    this.submitted = true;
    if (this.uploadAssignmentForm.invalid) {
      return;
    }
    this.assigmentData.assignmentName = this.uploadAssignmentForm.value.assignmentName;
    this.assigmentData.scheduledSubmissionDate = this.uploadAssignmentForm.value.assignmentDate;
    this.assigmentData.scheduledSubmissionDate = this.datepipe.transform(this.assigmentData.scheduledSubmissionDate, 'MM/dd/yyyy')
    this.assigmentData.courseId = this.courseId;
    this.assigmentData.topicId = this.topicId;
    this.assigmentData.assignmentsDesc = this.uploadAssignmentForm.value.descrip;
    this.meadifile = JSON.parse(this.contFile);
    this.assigmentData.media = this.meadifile.name;
    this.apiService.uploadAssignment(this.assigmentData).subscribe(res => {
   
  this.uploadResponse = res;
  if(this.uploadResponse.status == 200){
    Swal.fire({
      icon : 'success',
      text : this.uploadResponse.message
    });
  }
    else {
      Swal.fire({
        icon : 'warning',
        text : this.uploadResponse.message
      })
    }
    this.uploadAssignmentForm.reset();
    this.router.navigate(['/enrolled_student']);
  },error=>{
      Swal.fire({
        icon: 'warning',
        text: "Some thing went wrong",
      })
    });
}


}
