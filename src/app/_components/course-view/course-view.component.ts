import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator,MatSort } from '@angular/material';
import { ApiservicesService } from '../../_services/apiservices.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.scss']
})
export class CourseViewComponent implements OnInit {
  changeText: boolean;
  rowData:any;
  response : any;
  displayedColumns: string[] = ['courseTitle', 'coursePurpose', 'courseCoverage','eligibilityCriteria','certificateBenefits','courseFee','action'];
  isLoading = true;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.rowData.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;  

  constructor(public service : ApiservicesService) { }

  ngOnInit() {
    this.getCourseDetails()
  }

  changeTxt(){
    this.changeText = false;
    
  }

  getCourseDetails(){
      this.service.getCourseDetails().subscribe(res => { 
        this.rowData = new MatTableDataSource();
        this.isLoading = false;
        this.rowData.data= res.dataList;
        console.log(this.rowData.data)
        this.rowData.paginator = this.paginator;
        this.rowData.sort = this.sort;
      },error =>
      {
        this.isLoading = false
        swal.fire({
          icon: 'warning',
          text: "Some thing went wrong" ,
        })
      })
  }

  deleteCourse(courseId, courseTitle){
 
    swal.fire({
      title: 'You are about to delete ' + courseTitle + '?',
      text: "All the related contents will also be deleted",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#D3D3D3',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.service.deleteCourse(courseId).subscribe(res => {
          this.response = res; 
        const Toast = swal.mixin({
          toast: true,
          position: 'top',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
  
        Toast.fire({
          icon: 'success',
          title: this.response.message,
        })
        this.getCourseDetails();
      },error=>{
        swal.fire({
          icon: 'warning',
          text: "Some thing went wrong" ,
        })
      })
    }
  }) 
  }
 
}
