import { Component, OnInit } from '@angular/core';
import { ApiservicesService } from '../../../_services/apiservices.service';
import {MatTableDataSource} from '@angular/material/table';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';

@Component({
  selector: 'app-assemble',
  templateUrl: './assemble.component.html',
  styleUrls: ['./assemble.component.scss']
})
export class AssembleComponent implements OnInit {

  displayedColumns: string[] = ['check','topic','contants'];
  response:any;
  rowData:any;
  cordata:any;
  courseData:any;
  result:any;
  data:any;
  content:any;
  course:any;
  topic:any;
  courseId:any
  isLoading = true;

  assembleData =  {
    "contentId": "",
    "courseId": "",
    "topicId": ""
  }

  constructor(public service : ApiservicesService,private http:HttpClient) { }

  ngOnInit() {
    this.getmasterCourse();
    this.getApproveContent();
  }

getmasterCourse(){
  this.service.getmasterCourse().subscribe(res => {
   this.cordata = res;
   this.courseData = this.cordata.dataList;
   console.log(this.courseData)
   },error=>{
    swal.fire({
      icon: 'warning',
      text: "Some thing went wrong",
    })
  })
}

getApproveContent(){
  this.service.getApproveContent().subscribe(res => {
    this.response = res;
      this.rowData = new MatTableDataSource();
      this.isLoading = false;
      this.rowData.data= this.response.dataList;
      // console.log(this.rowData.data);
  },error => this.isLoading = false)
}

getcourseId(Id){
  this.courseId = Id;
  console.log(this.courseId)
}

getapprovedcontentbyID(){
  this.service.getapprovedcontentbyID(this.courseId).subscribe(res => {
    this.response = res;
      this.rowData = new MatTableDataSource();
      this.isLoading = false;
      this.rowData.data= this.response.dataList;
      // console.log(this.rowData.data);
  },error => this.isLoading = false)
}

// getid(id){

//   this.data =id;
 
//   this.content = this.data.contentId;
//   this.course = this.data.courses.courseId;
//   this.topic = this.data.topic.topicId;
//   console.log(this.content,this.course,this.topic)
// }

postassemble(){
  this.assembleData.contentId = this.content;
  this.assembleData.courseId = this.course;
  this.assembleData.topicId = this.topic;
  this.service.postassemble(this.listObj).subscribe(res => {
       this.result =res;
       if (this.result.status == 200){
        swal.fire({
          icon: 'success',
          text: this.result.message,
        })

          this.assembleData =  {
            "contentId": "",
            "courseId": "",
            "topicId": ""
          }
       }
      else{
      swal.fire({
        icon: 'warning',
        text: this.result.message,
      })
      }
       this.getApproveContent();
    },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong",
      })
      this.getApproveContent();
    })
    this.getApproveContent();
    this.listObj = [];
  }
 

 
listObj = [];
  setAll(event, element){
    this.data =element;
   if(event == true){
    if(this.listObj.length > 0){
    for(let i = 0; i<this.listObj.length; i++){
      if(this.listObj[i].topicId !=this.data.topic.topicId){
        this.listObj.push({"topicId" : this.data.topic.topicId, "courseId" :  this.data.courses.courseId, "contentId" : this.data.contentId})
    }
  }
}else{ this.listObj.push({"topicId" : this.data.topic.topicId, "courseId" :  this.data.courses.courseId, "contentId" : this.data.contentId})
}   
    }else{
      for(let i = 0; i<this.listObj.length; i++){
        if(this.listObj[i].topicId ==this.data.topic.topicId){
          this.listObj.splice(i, 1)
        }
      }
    }
  }
}
