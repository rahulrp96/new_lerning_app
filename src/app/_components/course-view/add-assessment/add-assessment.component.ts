import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-assessment',
  templateUrl: './add-assessment.component.html',
  styleUrls: ['./add-assessment.component.scss']
})
export class AddAssessmentComponent implements OnInit {

  assessementForm : FormGroup;
  couseDetails : any;
  courseDataList : [];
  courseId : number;
  submitted = false;
  assessmentRes : any;

  constructor(private formBuilder : FormBuilder, private apiService : ApiservicesService) { }
      
  ngOnInit() {
     this.assessementForm = this.formBuilder.group ({
      assessmentName : [null, Validators.required],
      minimumPassScore : [null, Validators.required],
      totalMark : [null, Validators.required],
      totalQuestion : [null, Validators.required],
      totalTestTime : [null, Validators.required]
     });

     this.getmasterCourse();
  }

  get f() { return this.assessementForm.controls; }

  getmasterCourse(){
    this.apiService.getmasterCourse().subscribe(res => {
     this.couseDetails = res;
     this.courseDataList = this.couseDetails.dataList;
     console.log(this.courseDataList)
  })
}

numberonly(event):boolean{
  const charcode=(event.which)?event.which:event.keycode;
  if(charcode>31 && (charcode<48 || charcode>57)){
  return false;
  }
  else
  return true;
}

  getcourseId(id){
    this.courseId = id;
  }

  addAssessment(){
    this.submitted = true;

    if(this.assessementForm.invalid){
      return;
    }
    this.assessementJson.assissmentName = this.assessementForm.value.assessmentName;
    this.assessementJson.courseId = this.courseId;
    this.assessementJson.createdBy = "LMS-WEB";
    this.assessementJson.minimumPassScore = this.assessementForm.value.minimumPassScore;
    this.assessementJson.totalMark = this.assessementForm.value.totalMark;
    this.assessementJson.totalQuestion = this.assessementForm.value.totalQuestion;
    this.assessementJson.totalTestTime = this.assessementForm.value.totalTestTime;

    this.apiService.addAssessment(this.assessementJson).subscribe(res => {
      this.assessmentRes = res;

      if(this.assessmentRes.status == 200){
        Swal.fire({
          icon : "success",
          text : this.assessmentRes.message
        });
      }else {
        Swal.fire({
          icon : "warning",
          text : this.assessmentRes.message
        })
      }
    }, error => {
      Swal.fire({
        icon : "warning",
        text : 'some thing went wrong'
      })

    });
    this.submitted = false;
    this.assessementForm.reset();
  }


  assessementJson = {
    "assissmentName": "string",
    "courseId": 0,
    "createdBy": "string",
    "minimumPassScore": 0,
    "totalMark": 0,
    "totalQuestion": 0,
    "totalTestTime": "string",
    
  }
}
