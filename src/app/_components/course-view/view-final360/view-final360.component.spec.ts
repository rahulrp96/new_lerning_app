import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFinal360Component } from './view-final360.component';

describe('ViewFinal360Component', () => {
  let component: ViewFinal360Component;
  let fixture: ComponentFixture<ViewFinal360Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFinal360Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFinal360Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
