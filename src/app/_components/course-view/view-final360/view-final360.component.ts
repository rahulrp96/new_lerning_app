import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import { ApproveCourse } from 'src/app/models/approve-course';
import swal from 'sweetalert2';

@Component({
  selector: 'app-view-final360',
  templateUrl: './view-final360.component.html',
  styleUrls: ['./view-final360.component.scss']
})
export class ViewFinal360Component implements OnInit {

  approveCourseForm: FormGroup;
  jobId : any;
  response : any=[];
  topics:any = [];
  contentId:any={};
  submitted = false;
  approveCourse : any;
  courseId : number;

  constructor(private route : ActivatedRoute,
    private formBuilder: FormBuilder, private service : ApiservicesService) { }

  ngOnInit() {

    this.approveCourseForm = this.formBuilder.group({
    courseFee: [null,  Validators.compose([Validators.required, Validators.minLength(3)])],
    extendedDays: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
    extensionFee: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
    rebateFreeStudent:[null, Validators.compose([Validators.required, Validators.minLength(3)])],
    rebatePaidStudent:[null, Validators.compose([Validators.required, Validators.minLength(3)])]
});
  this.route.params.subscribe(params => {
    this.courseId  = params.courseId;
    this.getCource( this.courseId)
  });
}

get f() { return this.approveCourseForm.controls; }

numberonly(event):boolean{
  const charcode=(event.which)?event.which:event.keycode;
  if(charcode>31 && (charcode<48 || charcode>57)){
  return false;
  }
  else
  return true;
}

getCource(id){
  this.service.viewFinal360(id).subscribe(res => {
  this.response = res;
  this.topics  = this.response.response.topics;
  console.log(this.topics)
  this.response = this.response.response; 
},error=>{
  swal.fire({
    icon: 'warning',
    text: "Some thing went wrong",
  })
})
}

deleteContentByContent(id){
  swal.fire({
    title: 'Are you sure?',
    text: "You want to delete",
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#D3D3D3',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      this.service.deleteConentByContentId(id).subscribe(res => {
        this.response = res; 
      const Toast = swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', swal.stopTimer)
          toast.addEventListener('mouseleave', swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'success',
        title: this.response.message,
      })
      this.getCource(this.courseId);
    },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong",
      })
    }
    )
  }
}) 
}

approveCourseAndUpdateCourse(){
  this.submitted = true;
  if (this.approveCourseForm.invalid) {
    return;
  }
  this.approveCourse = new ApproveCourse();
  this.approveCourse.courseFee = this.approveCourseForm.value.courseFee;
  this.approveCourse.extendedDays = this.approveCourseForm.value.extendedDays;
  this.approveCourse.extensionFee = this.approveCourseForm.value.extensionFee;
  this.approveCourse.rebateFreeStudent = this.approveCourseForm.value.rebateFreeStudent;
  this.approveCourse.rebatePaidStudent = this.approveCourseForm.value.rebatePaidStudent;
  this.service.approveCourseAndUpdateCourse(this.courseId , this.approveCourse).subscribe(res => {
    this.response = res;  
   if( this.response.status = 200){
    this.approveCourseForm.reset();
    swal.fire({
      icon: 'success',
      text: this.response.message,
    })   
   }
   else{
    this.approveCourseForm.reset();
    swal.fire({
      icon: 'warning',
      text: this.response.message,
    });
  }
  this.submitted = false
  },error=>{
    swal.fire({
      icon: 'warning',
      text: "Some thing went wrong",
    })
  });
}

}
