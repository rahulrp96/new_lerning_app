import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicWiseContentComponent } from './topic-wise-content.component';

describe('TopicWiseContentComponent', () => {
  let component: TopicWiseContentComponent;
  let fixture: ComponentFixture<TopicWiseContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicWiseContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicWiseContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
