import { Component, OnInit } from '@angular/core';
import { ApiservicesService } from '../../../_services/apiservices.service';
import {MatTableDataSource} from '@angular/material/table';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-topic-wise-content',
  templateUrl: './topic-wise-content.component.html',
  styleUrls: ['./topic-wise-content.component.scss']
})
export class TopicWiseContentComponent implements OnInit {

  data=null;
  faculty:any;
  facultyId:any;
  response :any;
  contentId : any;
  cordata:any;
  CourseId:any
  topdata:any;
  topicId:any;
  topiclist:any
  courseData:any;
  topicData:any;
  toBeApprovelist:any;
  rejectResponse : any;
  isLoading = true;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol','approve'];
 
  constructor(public service : ApiservicesService,private http:HttpClient) { }

  ngOnInit() {
    this.gettoBeApprove();
    this.getfaculty();
  }

  getfaculty(){
    this.service.getfaculty().subscribe(res => {
      this.faculty = res.dataList;
      //console.log(this.faculty)
     },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong" ,
      })
    })
  }

  getfacultyId(Id){
    this.facultyId = Id;
    //console.log(this.facultyId)
    this.getmasterCourse();
  }

  getmasterCourse(){
    this.service.getmasterCourse().subscribe(res => {
     this.cordata = res;
     this.courseData = this.cordata.dataList;
     if(this.cordata.dataList==null){
      alert("No data found");
     }
     //console.log(this.courseData)
     },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong" ,
      })
    })
  }

  getcourseId(Id){
    this.CourseId = Id;
    //console.log(this.CourseId)
    this.getmastertopics();
  }

  getmastertopics(){
    this.service.getmastertopics(this.CourseId).subscribe(res => {
     this.topdata = res;
     this.topicData = this.topdata.dataList;
     if(this.topdata.dataList==null){
      alert("No data found");
     }
    // console.log(this.topicData)
     },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong" ,
      })
    })
  }

  gettopicId(id){
    this.topicId = id;
    //console.log(this.CourseId)
    //this.getmatsercontent();
  }

  getMatserContent(){
   
    this.service.getMatserContent(this.topicId).subscribe(res => {
      this.topiclist = res;
      this.toBeApprovelist = new MatTableDataSource();
      this.isLoading = false;
      this.toBeApprovelist.data= this.topiclist.dataList;
     },error => this.isLoading = false)
  }

  gettoBeApprove(){
    this.service.gettoBeApprove().subscribe(res => {
      this.toBeApprovelist = new MatTableDataSource();
      this.isLoading = false;
      this.toBeApprovelist.data= res.dataList;
     },error => this.isLoading = false)
  }

  getContentId(id){
   
    this.contentId = id;
    //console.log(this.contentId);
    document.getElementById('approveTopic').style.display='block'
    
  }

  putApprove(id){
    this.service.putApprove(this.contentId,this.data).subscribe(res => {
      this.response = res; 
      if( this.response.status = 200){
        swal.fire({
          icon: 'success',
          text: this.response.message,
          
        })   
        
       }
       else{
        swal.fire({
          icon: 'warning',
          text: this.response.message,
        });
      }
      document.getElementById('approveTopic').style.display='none'
      this.getMatserContent()
      this.gettoBeApprove()
     },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong" ,
      })
    })

   

  }

  rejectContent(contentId){
    swal.fire({
      title: 'Are you sure?',
      text: "You want to reject",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#D3D3D3',
      confirmButtonText: 'Yes, reject it!'
    }).then((result) => {
      if (result.value) {
        this.service.rejectContent(contentId).subscribe(res => {
          this.rejectResponse = res; 
        const Toast = swal.mixin({
          toast: true,
          position: 'top',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
  
        Toast.fire({
          icon: 'success',
          title: 'Content rejected successfully',
        })
        this.gettoBeApprove();
      })
    }
  }) 
      //console.log(this.rejectResponse)
}

}
