import { Component, OnInit } from '@angular/core';
import { ApiservicesService } from '../../../_services/apiservices.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-new-course',
  templateUrl: './add-new-course.component.html',
  styleUrls: ['./add-new-course.component.scss']
})
export class AddNewCourseComponent implements OnInit {
  registerForm: FormGroup;
  id:any;
  i:any;
  images:any;
  file:any;
  narration:any;
  result:any;
  resulttopic:any;
  topicId:any;
  courseId:any;
  contFile:any;
  contType:any;
  isVisible = false;
  submitted = false;
  submitt = false;
  errormessage = '';
  tableForm: FormGroup;
  list:any=[];
  topicnm:any;
  topicdes:any;
  showtable=false;
  shtable=false;


  topicdata = {
    "id": "",
    "topicName": "",
    "description":""
  }

  constructor(private formBuilder: FormBuilder,private http:HttpClient,
  public service : ApiservicesService,private router: Router) { }

  ngOnInit() {
    this.i=[]
    this.registerForm = this.formBuilder.group({
      courseTitle: [null,Validators.compose([Validators.required,Validators.minLength(3)])],
      coursePurpose: [null,Validators.required],
      objective: [''],
      eligibilityCriteria:[null,Validators.required],
      duration:[''],
      narration:[''],
      extension:[''],
      certificateBenefits:[null,Validators.required],
      courseCoverage:[''],
      CertificateFile:[''],
      courseImage:['',Validators.required],
      courseCertificate:['',Validators.required],
  });
  this.id="";
  this.narration="";
  this.images="";
  this.file="";
  this.contFile="";
  this.courseId="";
  this.topicId="";


    this.tableForm = this.formBuilder.group({
    topicname: [''],
    topicDescrip: [''],
  });

}

get f() { return this.registerForm.controls; }

  selectimage(event){
    if(event.target.files.length > 0){
      const file =event.target.files[0]
      this.images =file;
      console.log(this.images)
    }
 } 

 selectFile(event){
  if(event.target.files.length > 0){
    const file =event.target.files[0]
    this.file =file;
    console.log(this.file)
  }
} 

postCourse(){
  //this.isVisible = true;
  this.submitted = true;
  if (this.registerForm.invalid) {
    return;
  }
  
  const formDatas = new FormData();
  formDatas.append('categoryId',this.id);
  formDatas.append('certificateBenefits',this.registerForm.value.certificateBenefits);
  formDatas.append('courseCertificate',this.file);
  formDatas.append('courseCoverage',this.registerForm.value.courseCoverage);
  formDatas.append('courseImage',this.images);
  formDatas.append('coursePurpose',this.registerForm.value.coursePurpose);
  formDatas.append('courseTitle',this.registerForm.value.courseTitle);  
  formDatas.append('eligibilityCriteria',this.registerForm.value.eligibilityCriteria);
  formDatas.append('id',this.id)
  formDatas.append('narration',this.registerForm.value.narration);
  formDatas.append('duration', this.registerForm.value.duration);
   this.service.postCourse(formDatas).subscribe((res)=> {
    this.result =res;
     if (this.result.status==200){
      this.courseId = this.result.data.courseId;
      swal.fire({
        icon: 'success',
        text: this.result.message,
      })   
     }
     else{
      swal.fire({
        icon: 'warning',
        text: this.result.message,
      })
    }
    this.submitted = false;
    this.isVisible = true;
  },error=>{
    swal.fire({
      icon: 'warning',
      text: "Some thing went wrong",
    })
  }
  )
}

  filedInc(data)
  {
    if(this.shtable==false){
      this.i =[];
    }
    this.shtable=true;

    this.i.push({'user':data})
  }

  postTopic(){
    this.topicdata.topicName=this.tableForm.value.topicname;
    this.topicdata.description=this.tableForm.value.topicDescrip;
    if(this.topicdata == null || this.topicdata.topicName == null || this.topicdata.topicName==''){
      this.errormessage = 'topic is required!';
      return;
    }
    this.errormessage = '';
    this.service.posttopic(this.courseId, this.topicdata).subscribe((res)=> {
       this.resulttopic =res;
       if (this.resulttopic.status == 200){
        this.topicId = this.resulttopic.data.topicId
        this.topicnm = this.resulttopic.data.topicName
        this.topicdes = this.resulttopic.data.description
        console.log(this.resulttopic.data)
        this.list.push({'topic': this.topicnm,'description':this.topicdes})
        console.log(this.list)
        swal.fire({
          icon: 'success',
          text: this.resulttopic.message,
        })
        this.showtable=true;
        this.tableForm.reset();
        this.shtable=false;
       }
      else{
      swal.fire({
        icon: 'warning',
        text: this.resulttopic.message,
      })
      }
    },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong",
      })
    }
    )
  }

  selectCont(event){
    if(event.target.files.length > 0){
      for (var i = 0; i < event.target.files.length; i++) {
      const file =event.target.files[i]
      this.contFile =file;
      }
      console.log(this.contFile)
    }
  }

  fileType(data){
    
    this.contType = data;
    //console.log(this.contType);
  }

  postContent(){  
    console.log(this.courseId,this.topicId)
    const formData = new FormData();
    formData.append('contentType',this.contType);
    formData.append('contentValue',this.contType);
    formData.append('media',this.contFile); 
    this.service.postContent(this.courseId, this.topicId, formData).subscribe((res)=> {
      this.isVisible = true;
       this.resulttopic =res;
       if (this.resulttopic.status==200){
        swal.fire({
          icon: 'success',
          text: this.resulttopic.message,
        })
       }
      else{
      swal.fire({
        icon: 'warning',
        text: this.resulttopic.message,
      })
      }
      document.getElementById('id01').style.display='none'
      this.router.navigate(['/course']);
    },error=>{
      swal.fire({
        icon: 'warning',
        text: "Some thing went wrong" ,
      })
    }
    )
  }


}


