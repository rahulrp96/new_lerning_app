import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionEnrolmentComponent } from './admission-enrolment.component';

describe('AdmissionEnrolmentComponent', () => {
  let component: AdmissionEnrolmentComponent;
  let fixture: ComponentFixture<AdmissionEnrolmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmissionEnrolmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmissionEnrolmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
