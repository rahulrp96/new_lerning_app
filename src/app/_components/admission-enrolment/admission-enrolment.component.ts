import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  name: string;
  amount: string;
  status: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { name: 'Java', amount: 'Paid', status: 'Admitted'},
  { name: 'PHP', amount: 'Not paid', status: 'Non Admit'},
  { name: 'Java Tech', amount: 'Paid', status: 'Admitted'},
  { name: 'PHP', amount: 'Paid', status: 'Non Admit'},
];

@Component({
  selector: 'app-admission-enrolment',
  templateUrl: './admission-enrolment.component.html',
  styleUrls: ['./admission-enrolment.component.scss']
})
export class AdmissionEnrolmentComponent implements OnInit {

  displayedColumns: string[] = ['Name', 'name', 'amount', 'email', 'Mobile', 'status', 'action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor() { }

  ngOnInit() {
  }

}
