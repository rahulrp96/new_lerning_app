import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
declare var $ : any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'lms';

  showHead: boolean = false;

  constructor(private router: Router) {
    router.events.forEach((event) => { 
      if (event instanceof NavigationStart) {
       
        if (event['url'] == '/login' || event['url'] == '/' || event['url'] == '/signin') {
          $("#mySidenav").css('display','none');
          $("#header_main").css('display','none');
          $("#footers").css('display','none');
        } 
      }

      if (event instanceof NavigationStart) {
        if (event['url'] == '/dashboard') {
          $("#mySidenav").css('display','block');
          $("#header_main").css('display','block');
          $("#footers").css('display','block');
        } 
      }
    });
  }
}
