import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ApiservicesService } from './_services/apiservices.service';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgxSocialLoginModule } from 'ng8-social-login';
import { DatePipe } from '@angular/common';

// component Added 
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './_components/login/login.component';
import { SigninComponent } from './_components/signin/signin.component';
import { HeaderComponent } from './_layout/header/header.component';
import { FooterComponent } from './_layout/footer/footer.component';
import { SidebarComponent } from './_layout/sidebar/sidebar.component';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { CourseViewComponent } from './_components/course-view/course-view.component';
import { AddNewCourseComponent } from './_components/course-view/add-new-course/add-new-course.component';
import { AddContentComponent } from './_components/course-view/add-content/add-content.component';
import { ViewFinal360Component } from './_components/course-view/view-final360/view-final360.component';
import { TopicWiseContentComponent } from './_components/course-view/topic-wise-content/topic-wise-content.component';
import { AssembleComponent } from './_components/course-view/assemble/assemble.component';
import { AssignmentComponent } from './_components/enrolled-student/assignment/assignment.component';

import { EnrolledStudentComponent } from './_components/enrolled-student/enrolled-student.component';
import { AdmissionEnrolmentComponent } from './_components/admission-enrolment/admission-enrolment.component';
import { MatFileUploadModule } from 'angular-material-fileupload';
// News
import { NewsComponent } from './_components/news/news.component';
import { AddNewsComponent } from './_components/news/add-news/add-news.component';
import { EditNewsComponent } from './_components/news/edit-news/edit-news.component';

// UserComponent
import { MyCoursesComponent } from './_paiduser/userComponents/my-courses/my-courses.component';
import { PaidUserSidebarComponent } from './_layout/paid_user_layout/sidebar/paid-user-sidebar/paid-user-sidebar.component';
import { UserheaderComponent } from './_paiduser/userLayout/userheader/userheader.component';
import { UserfooterComponent } from './_paiduser/userLayout/userfooter/userfooter.component';
import { UsersideNavComponent } from './_paiduser/userLayout/userside-nav/userside-nav.component';
import { CourseMaterialComponent } from './_paiduser/userComponents/course-material/course-material.component';
import { AssignmetListComponent } from './_paiduser/userComponents/assignmet-list/assignmet-list.component'; 
import { NotificationComponent } from './_paiduser/notification/notification.component';
import { OtherCourseComponent } from './_paiduser/other-course/other-course.component';
import { AddQuestAnsComponent } from './_paiduser/add-quest-ans/add-quest-ans.component';


// Material Link Added
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatTableModule } from '@angular/material/table';
import { MatDatepicker, MatDatepickerInput, MatDatepickerModule, MatExpansionModule, MatInputModule, MatNativeDateModule, MatPaginatorModule, MatProgressSpinnerModule,MatRadioButton,MatRadioModule,MatSortModule } from "@angular/material";
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatContenteditableModule } from 'mat-contenteditable';
import { AddAssessmentComponent } from './_components/course-view/add-assessment/add-assessment.component';
import { AssessmentComponent } from './_components/assessment/assessment.component';
import { AssessmentListviewComponent } from './_paiduser/assessment-listview/assessment-listview.component';
import { AddFacultyComponent } from './_components/add-faculty/add-faculty.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SigninComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    DashboardComponent,
    CourseViewComponent,
    AddNewCourseComponent,
    AddContentComponent,
    ViewFinal360Component,
    TopicWiseContentComponent, 
    AssembleComponent,
    AssignmentComponent,
   
    EnrolledStudentComponent,
    AdmissionEnrolmentComponent,

    // user
    MyCoursesComponent,
    PaidUserSidebarComponent,
    UserheaderComponent,
    UserfooterComponent,
    UsersideNavComponent,
    CourseMaterialComponent,
    AssignmetListComponent,
    
    // News
    NewsComponent,
    AddNewsComponent,
    EditNewsComponent,
    NotificationComponent,
    OtherCourseComponent,
    AddQuestAnsComponent,
   
    AddAssessmentComponent,
    AssessmentComponent,
    AssessmentListviewComponent,
    AddFacultyComponent
   
  
   
  ],
  imports: [
    NgxSocialLoginModule.init( {
      google: {
          client_id: '578918760508-v348dqehqi556epod7oofvfb08leofk6.apps.googleusercontent.com'
      },
      facebook: {
          initOptions: {
              appId: '383125889380997'
          }
      }
  }
),
    BrowserModule,
    AppRoutingModule,
    MatFileUploadModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    //SocialLoginModule,
    
    // materials
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSortModule,
    MatIconModule,
    MatCheckboxModule,
    MatMenuModule,
    MatSidenavModule,
    CKEditorModule,
    MatFormFieldModule,
    MatContenteditableModule,
    MatSidenavModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule

    
    
  ],
  providers: [ApiservicesService, DatePipe],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginComponent,
    SigninComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    DashboardComponent,
    CourseViewComponent,
    AddNewCourseComponent,
    AddContentComponent,
  ]
})
export class AppModule { }
