import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaidUserSidebarComponent } from './paid-user-sidebar.component';

describe('PaidUserSidebarComponent', () => {
  let component: PaidUserSidebarComponent;
  let fixture: ComponentFixture<PaidUserSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaidUserSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaidUserSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
