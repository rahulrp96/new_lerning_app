import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { SocialLoginService } from 'ng8-social-login';

declare var $ : any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userDetails:any;
  user:any;
  value :any;
  imgNotVisible : boolean;
  imgVisible : boolean;
  name : string;
  profileImg : string
  isShow = false;
  constructor(private router : Router, private socialService : SocialLoginService) {}

  ngOnInit() {
    this.dynamicHeader();
    let userss = localStorage.getItem('user');
    this.imgVisible = userss == 'null' ? true : false;
   
   if( !this.imgVisible ){
    
    this.user =  JSON.parse(localStorage.getItem('user'));
  
    if(this.user.name == null || this.user.name == '' || this.user.name == undefined){
      this.name = '';
    }else{ 
    this.name = this.user.name;
    }
    if(this.user.profileImg == null || this.user.profileImg == '' || this.user.profileImg == undefined){
      this.profileImg = '../assets/images/faces/face1.jpg';
    }else{ 
      this.profileImg = this.user.profileImg;
    }
  }else{
    this.profileImg = '../assets/images/faces/face1.jpg'
    let namess = localStorage.getItem('name');
    this.name = namess;
  }
    //this.user = this.userDetails.user;
  
}

  dynamicHeader(){
    
    if(this.router.url == '/dashboard' || this.router.url == '/dashboard;login=%5Bobject%20Object%5D'){
      this.value = "Dashboard";
    }
    else if(this.router.url == '/course'){
      this.value = "Courses";
    }
    else if(this.router.url == '/addcourse'){
      this.value = "Add Course";
    }
    else if(this.router.url == '/addcontent'){
      this.value = "Add Content";
    }

   
    else if(this.router.url.includes( '/view360')){
      this.value = "360view";
    }
    else if(this.router.url == '/lessons'){
      this.value = "Content Approvals";
    }
    else if(this.router.url == '/combinevideos'){
      this.value = "Combine Videos";
    }
    else if(this.router.url == '/assignment'){
      this.value = "Assignment";
    }
    else if(this.router.url == '/add_assessment'){
      this.value = "Add Assessment";
    }
    else if(this.router.url.includes('/assessment/')){
      this.value = "Assessment";
    }
    else if(this.router.url == '/enrolled_student'){
      this.value = "Enrolled Student";
    }
    else if(this.router.url == '/admission_enrolment'){
      this.value = "Admission and Enrolments";
    }
    else if(this.router.url == '/news'){
      this.value = "News";
    }

    else if(this.router.url.includes('/add_question_ans')){
      this.value = "Add Questions/Answers";
    }

    else if(this.router.url == '/assessment_list_view'){
      this.value = "Assessment List View";
    }

    else if(this.router.url == '/assignment_list_view'){
      this.value = "Assignment List View";
    }
    
    else if(this.router.url == '/add_faculty'){
      this.value = "Add Faculty";
    }
  }

  logout(){
    localStorage.clear();   
     this.socialService.logout();
    this.router.navigate(['/']);
  } 

  
}
