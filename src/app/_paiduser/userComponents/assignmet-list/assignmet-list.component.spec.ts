import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmetListComponent } from './assignmet-list.component';

describe('AssignmetListComponent', () => {
  let component: AssignmetListComponent;
  let fixture: ComponentFixture<AssignmetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
