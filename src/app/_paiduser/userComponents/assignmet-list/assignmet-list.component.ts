import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  name: string;
  date: any;
  lastdate: any;
  downStatus: any;
  sumStatus:any;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {name: 'Core Java', date: 'Aug 25, 2020', lastdate:'Aug 25, 2020', downStatus: 'Downloaded Aug 27, 2020', sumStatus:'Submitted Aug 25,2019'},
  {name: 'Hibernate', date: 'Sep 15, 2020', lastdate:'Aug 25, 2020', downStatus: '', sumStatus:'Download Pending'},
  {name: 'SpringBoot', date: 'Sep 10, 2020', lastdate:'Aug 25, 2020', downStatus: 'Downloaded Aug 25, 2020', sumStatus:''},
];

@Component({
  selector: 'app-assignmet-list',
  templateUrl: './assignmet-list.component.html',
  styleUrls: ['./assignmet-list.component.scss']
})
export class AssignmetListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'date', 'lastdate', 'downStatus', 'sumStatus'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor() { }

  ngOnInit() {
  }

}
