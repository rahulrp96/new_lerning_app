import { Component, OnInit } from '@angular/core';

import { ApiservicesService } from 'src/app/_services/apiservices.service';

@Component({
  selector: 'app-my-courses',
  templateUrl: './my-courses.component.html',
  styleUrls: ['./my-courses.component.scss']
})
export class MyCoursesComponent implements OnInit {
  myCoursesResponse : any;
  dataList = [];

  constructor(private apiService : ApiservicesService) { }


  ngOnInit() {
   let userId =  localStorage.getItem('userId');

    this.myCourses(userId)
  }



myCourses(userId){
  this.apiService.myCourses(userId).subscribe(res => {
    this.myCoursesResponse = res;
    this.dataList = this.myCoursesResponse.dataList;
    });
    
}
}
