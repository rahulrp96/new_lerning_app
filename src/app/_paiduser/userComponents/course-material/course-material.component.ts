import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiservicesService } from '../../../_services/apiservices.service';

@Component({
  selector: 'app-course-material',
  templateUrl: './course-material.component.html',
  styleUrls: ['./course-material.component.scss']
})
export class CourseMaterialComponent implements OnInit {
  submitted = false;
  courseDetail:any=[];
  course:any;
  topics:any=[];

  constructor( public service : ApiservicesService, private activeRoute : ActivatedRoute) { }

  ngOnInit() {
      this.lectures();
      
  this.activeRoute.params.subscribe(param => {
    this.getCourseDetail(param.courseId);
  })
  
  }

  getCourseDetail(courseId){
    this.service.getCourseDetail(courseId).subscribe(res => {
      this.course = res;
      this.courseDetail = this.course.response;
      this.topics = this.course.response.topics;
      console.log(this.courseDetail);
     })
  }


  lectures(){
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }
  }
    
}
