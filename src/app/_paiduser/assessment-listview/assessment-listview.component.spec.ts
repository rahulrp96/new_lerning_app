import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentListviewComponent } from './assessment-listview.component';

describe('AssessmentListviewComponent', () => {
  let component: AssessmentListviewComponent;
  let fixture: ComponentFixture<AssessmentListviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentListviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentListviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
