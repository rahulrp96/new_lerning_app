import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-assessment-listview',
  templateUrl: './assessment-listview.component.html',
  styleUrls: ['./assessment-listview.component.scss']
})
export class AssessmentListviewComponent implements OnInit {
  data:any;
  rowData:any;
  response : any;
 
  displayedColumns: string[] = ['assessmentName','courseName','totalQuestion','totalMark', 'minimumPassScore', 'totalTestTime', 'createdDate','action'];
  isLoading = true;
 
  constructor(private apiService : ApiservicesService) { }

  ngOnInit() {

    this.findAllAssesment();
  }

  findAllAssesment(){
    this.apiService.findAllassessmentList().subscribe(res => {
      this.data = res;
      this.rowData = new MatTableDataSource();
        this.isLoading = false;
        this.rowData.data=  this.data.dataList;
        console.log(this.rowData.data)
      },error =>
      {
        this.isLoading = false
        Swal.fire({
          icon: 'warning',
          text: "Some thing went wrong" ,
        })
      })
  }

    
deleteAssessment(assissmentId, assissmentName){
  Swal.fire({
    title: 'You are about to delete ' + assissmentName + '?',
    text: "",
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#D3D3D3',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
    //   this.service.deleteCourse(courseId).subscribe(res => {
    //     this.response = res; 
    //   const Toast = swal.mixin({
    //     toast: true,
    //     position: 'top',
    //     showConfirmButton: false,
    //     timer: 3000,
    //     timerProgressBar: true,
    //     onOpen: (toast) => {
    //       toast.addEventListener('mouseenter', Swal.stopTimer)
    //       toast.addEventListener('mouseleave', Swal.resumeTimer)
    //     }
    //   })

    //   Toast.fire({
    //     icon: 'success',
    //     title: this.response.message,
    //   })

    // },error=>{
    //   Swal.fire({
    //     icon: 'warning',
    //     text: "Some thing went wrong" ,
    //   })
    // })
  }
}) 
}

}
