import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  registerForm:FormGroup;
  submitted = false;
  constructor(private formBuilder : FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      notificationName: [null,Validators.compose([Validators.required,Validators.minLength(3)])],
      description :['']
  });
  }

}
