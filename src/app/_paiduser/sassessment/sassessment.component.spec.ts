import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SassessmentComponent } from './sassessment.component';

describe('SassessmentComponent', () => {
  let component: SassessmentComponent;
  let fixture: ComponentFixture<SassessmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SassessmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SassessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
