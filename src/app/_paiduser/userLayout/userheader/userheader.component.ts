import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { SocialLoginService } from 'ng8-social-login';
declare var $ : any;

@Component({
  selector: 'app-userheader',
  templateUrl: './userheader.component.html',
  styleUrls: ['./userheader.component.scss']
})
export class UserheaderComponent implements OnInit {
  value :any;
  imgVisible: boolean;

user: any;
name: any;
profileImg:any;
  constructor(private router : Router, private socialService : SocialLoginService) { }


  ngOnInit() {
    this.dynamicHeader();
    let userss = localStorage.getItem('user');
    this.imgVisible = userss == 'null' ? true : false;
   
   if( !this.imgVisible ){
    
    this.user =  JSON.parse(localStorage.getItem('user'));

    if(this.user.name == null || this.user.name == '' || this.user.name == undefined){
      this.name = '';
    }else{ 
    this.name = this.user.name;
    }
    if(this.user.profileImg == null || this.user.profileImg == '' || this.user.profileImg == undefined){
      this.profileImg = '../assets/images/faces/face1.jpg';
    }else{ 
      this.profileImg = this.user.profileImg;
    }
  }else{
    this.profileImg = '../assets/images/faces/face1.jpg'
    let namess = localStorage.getItem('name');
    this.name = namess;
  }
    //this.user = this.userDetails.user;
  
  }

  dynamicHeader(){
    if(this.router.url == '/myCourses'){
      this.value = "My Courses";
    }
    else if(this.router.url == '/assignmetList'){
      this.value = "Assignments";
    }
    else if(this.router.url == '/courseMaterial'){
      this.value = "Course Details";
    }
    else if(this.router.url == '/otherCourse'){
      this.value = "Courses - 1";
    }
    else if(this.router.url == '/question-ans'){
      this.value = "Assessment";
    }
    else if(this.router.url == '/userassessment'){
      this.value = "Assessment";
    }
    else if(this.router.url == '/notification'){
      this.value = "Add Notifications";
    }
  }

  logout(){
    localStorage.clear();   
     this.socialService.logout();
    this.router.navigate(['/']);
  } 

}
