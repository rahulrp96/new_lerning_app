import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQuestAnsComponent } from './add-quest-ans.component';

describe('AddQuestAnsComponent', () => {
  let component: AddQuestAnsComponent;
  let fixture: ComponentFixture<AddQuestAnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddQuestAnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQuestAnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
