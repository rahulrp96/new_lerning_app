import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiservicesService } from 'src/app/_services/apiservices.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-quest-ans',
  templateUrl: './add-quest-ans.component.html',
  styleUrls: ['./add-quest-ans.component.scss']
})
export class AddQuestAnsComponent implements OnInit {
  answers= false;
  questions = false;
  questionForm:FormGroup;
  answerForm : FormGroup;
  register:FormGroup;
  submitted = false;
  submitt = false;
  assessmentId : number;
  questionsRespons : any;
  correctAns : string;
  questionId : number;
  answerRes : any;
  images : any;
  questionImg : any;
  answerImg1 : any;
  answerImg2 : any;
  answerImg3 : any;
  answerImg4 : any;
  answerOptionDtos : any;

  constructor(private formBuilder : FormBuilder,private route : ActivatedRoute, private apiService : ApiservicesService) { }

  ngOnInit() {
    this.lectures();
    this.questionForm = this.formBuilder.group({
      question: [null,Validators.compose([Validators.required,Validators.minLength(3)])],
      questionImage: ['']
  });

  this.answerForm = this.formBuilder.group({
    answer1: [null,Validators.compose([Validators.required,Validators.minLength(3)])],
    answer2: [null,Validators.compose([Validators.required,Validators.minLength(3)])],
    answer3: [null, Validators.compose([Validators.required,Validators.minLength(3)])],
    answer4: [null, Validators.compose([Validators.required,Validators.minLength(3)])],
    isCorrect: [''],
    answerImage1 : [''],
    answerImage2 : [''],
    answerImage3 : [''],
    answerImage4 : ['']
  });

  this.questionImg ='';
  this.answerImg1 ='';
  this.answerImg2 ='';
  this.answerImg3 ='';
  this.answerImg4 ='';
  this.images = '';

  this.route.params.subscribe(res => {
   this.assessmentId = res.assessmentId;
  })
}

  get f() { return this.questionForm.controls;}


  addQuestions(){
    this.submitted = true;
    if (this.questionForm.invalid) {
      return;
    }
    
  const formData = new FormData();
  //formData.append("photoUrl", null);
  formData.append("questionDescription", this.questionForm.value.question);
  if(this.questionImg != null && this.questionImg != undefined && this.questionImg != ''){
    formData.append("questionImage", this.questionImg);
  }
 
    this.apiService.addQuestions(formData, this.assessmentId).subscribe( res => {
      this.questionsRespons = res;
      if(this.questionsRespons.status == 200){
       this.questionId = (this.questionsRespons.data != null) ?  this.questionsRespons.data.questionId : 0;
        Swal.fire({
          icon : "success",
          text : this.questionsRespons.message
        })
      }else {
        Swal.fire( {
          icon : "warning",
          text : this.questionsRespons.message
        })
      }

    },error => {
      Swal.fire( {
        icon : "warning",
        text : 'some thing went wrong'
      });
  });
}

isCorrectAns(correctAns){
 this.correctAns = correctAns;
}
 
addAnswers(){
  this.submitted = true;
  if (this.answerForm.invalid) {
    return;
  }
  if(this.questionId != null && this.questionId !=0 ){
    
    const formData = new FormData();
    this.answerOptionDtos = [
      {"answerImage" : this.answerImg1, "answerValue" : this.answerForm.value.answer1 ,"isCorrect" : (null != this.correctAns && this.correctAns!= '' && this.correctAns != undefined && this.correctAns == 'answer1') ? 'Y' : ''},
      {"answerImage" : this.answerImg2, "answerValue" : this.answerForm.value.answer2 ,"isCorrect" : (null != this.correctAns && this.correctAns!= '' && this.correctAns != undefined && this.correctAns == 'answer2') ? 'Y' : ''},
      {"answerImage" : this.answerImg3, "answerValue" : this.answerForm.value.answer3 ,"isCorrect" : (null != this.correctAns && this.correctAns!= '' && this.correctAns != undefined && this.correctAns == 'answer3') ? 'Y' : ''},
      {"answerImage" : this.answerImg4, "answerValue" : this.answerForm.value.answer4 ,"isCorrect" : (null != this.correctAns && this.correctAns!= '' && this.correctAns != undefined && this.correctAns == 'answer4') ? 'Y' : ''}
    ];
   
    for(let i = 0; i < 4; i++) {
      formData.append("answerValue", this.answerOptionDtos[i].answerValue);
      formData.append("answerImage", this.answerOptionDtos[i].answerImage);
      formData.append("isCorrect", this.answerOptionDtos[i].isCorrect);
  }

    this.apiService.addAnswers(formData, this.questionId).subscribe(res => {
      this.answerRes = res;
      if(this.answerRes.status == 200){
        Swal.fire( {
          icon : "success",
          text : this.answerRes.message
        })
      }else {
        Swal.fire( {
          icon : "warning",
          text : this.answerRes.message
        })
      }

    },error => {
      Swal.fire( {
        icon : "warning",
        text : 'some thing went wrong'
      });
    });
  }else {
    Swal.fire( {
      icon : "warning",
      text : 'please add question before add answer!'
    })
  }
  }

selectimage(event, type){
  if(type != null &&  type!= '' && type != undefined){
    ///this.images = "";
    if(event.target.files.length > 0){
      const file =event.target.files[0]
      //this.images =file;
      this.questionImg = (type=='question') ? file : this.questionImg ;
      this.answerImg1 = (type=='answer1') ? file : this.answerImg1 ;
      this.answerImg2 = (type=='answer2') ? file : this.answerImg2 ;
      this.answerImg3 = (type=='answer3') ? file :this.answerImg3 ;
      this.answerImg4 = (type=='answer4') ? file : this.answerImg4;
      //console.log(this.images)
    }
  }
 } 

 
   
 lectures(){
  var acc = document.getElementsByClassName("accordion");
  var i;
  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      } 
    });
  }
}
  
 

}
