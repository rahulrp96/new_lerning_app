import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './_components/login/login.component';
import { SigninComponent } from './_components/signin/signin.component';

// Admin Componenets
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { CourseViewComponent } from './_components/course-view/course-view.component';
import { AddNewCourseComponent } from './_components/course-view/add-new-course/add-new-course.component';
import { AddContentComponent } from './_components/course-view/add-content/add-content.component';
import { ViewFinal360Component } from './_components/course-view/view-final360/view-final360.component';
import { TopicWiseContentComponent } from './_components/course-view/topic-wise-content/topic-wise-content.component';
import { AssembleComponent } from './_components/course-view/assemble/assemble.component';
import { AssignmentComponent } from './_components/enrolled-student/assignment/assignment.component';
import { EnrolledStudentComponent } from './_components/enrolled-student/enrolled-student.component';
import { AdmissionEnrolmentComponent } from './_components/admission-enrolment/admission-enrolment.component';

// News
import { NewsComponent } from './_components/news/news.component';
import { AddNewsComponent } from './_components/news/add-news/add-news.component';
import { EditNewsComponent } from './_components/news/edit-news/edit-news.component';

// Student Componenet
import { MyCoursesComponent } from './_paiduser/userComponents/my-courses/my-courses.component';
import { CourseMaterialComponent } from './_paiduser/userComponents/course-material/course-material.component';
import { AssignmetListComponent } from './_paiduser/userComponents/assignmet-list/assignmet-list.component'; 
import { NotificationComponent } from './_paiduser/notification/notification.component';
import { OtherCourseComponent } from './_paiduser/other-course/other-course.component';
import { AddQuestAnsComponent } from './_paiduser/add-quest-ans/add-quest-ans.component';
import { AddAssessmentComponent } from './_components/course-view/add-assessment/add-assessment.component';
import { AssessmentComponent } from './_components/assessment/assessment.component';
import { AssessmentListviewComponent } from './_paiduser/assessment-listview/assessment-listview.component';
import { AddFacultyComponent } from './_components/add-faculty/add-faculty.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'course', component: CourseViewComponent },
  { path: 'addcourse', component: AddNewCourseComponent },
  { path: 'addcontent', component: AddContentComponent },
  { path: 'view360/:courseId', component: ViewFinal360Component },
  { path: 'lessons', component: TopicWiseContentComponent },
  { path: 'combinevideos', component: AssembleComponent },
  { path: 'assignment', component: AssignmentComponent }, 
  { path: 'enrolled_student', component: EnrolledStudentComponent },
  { path: 'admission_enrolment', component: AdmissionEnrolmentComponent },

  // student path
  { path: 'myCourses', component: MyCoursesComponent },
  { path: 'courseMaterial/:courseId', component: CourseMaterialComponent },
  { path: 'assignmetList', component: AssignmetListComponent },
  { path: 'notification', component: NotificationComponent },
  { path: 'otherCourse', component: OtherCourseComponent },
  { path: 'add_question_ans/:assessmentId', component: AddQuestAnsComponent },
  { path: 'add_assessment', component: AddAssessmentComponent },
  { path: 'assessment/:assessmentId', component: AssessmentComponent },
  { path: 'assessment_list_view', component: AssessmentListviewComponent },
  { path: 'assignment_list_view', component: AssignmetListComponent },
  { path: 'add_faculty', component: AddFacultyComponent }, 

  // News
  { path: 'news', component: NewsComponent },
  { path: 'addnews', component: AddNewsComponent },
  { path: 'editnews/:newsId', component: EditNewsComponent, pathMatch: 'full' },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
