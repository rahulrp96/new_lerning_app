import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiservicesService {
  private _baseUrl = environment.baseUrl;

  private _getCourseDetails = this._baseUrl + 'courses/course';
  private _postCourse = this._baseUrl + 'courses/course';
  private _loginUser = this._baseUrl + 'ApiGateway/login';
  private _getViewFinal300 = this._baseUrl + 'courses/course/';
  private _deleteContet = this._baseUrl + 'courses/course/content/';
  private _approveCourse = this._baseUrl + 'courses/course/';
  private _gettoBeApprove = this._baseUrl + 'courses/course/contents/toBeApprove';
  private _posttopic = this._baseUrl + 'courses/topic/';
  private _putApprove = this._baseUrl + '/courses/course/content/';
  private _getApproveContent = this._baseUrl + 'courses/course/contents/approved';
  private _getmasterCourse = this._baseUrl + 'courses/matser/course';
  private _getmastertopics = this._baseUrl + 'courses/matser/';
  private _postassemble = this._baseUrl + 'courses/course/assemble';
  private _getfaculty = this._baseUrl +'faculty/faculty/allFaculty';
  private _getmatsercontent = this._baseUrl +'courses/matser/';
  private _rejectContent = this._baseUrl + 'courses/course/content/';
  private _getapprovedcontentbyID  = this._baseUrl +'courses/course/contents/approved/';
  private _getfetchAssignment  = this._baseUrl +'PaidStudent/fetchAssignment/1/topic/1';
  private _uploadAssignment = this._baseUrl + 'faculty/assignment/create'
  private _viewNews = this._baseUrl + 'news/news'
  private _deleteNews = this._baseUrl + 'news/news/'
  private _editNews = this._baseUrl + 'news/news/';
  private _addNews = this._baseUrl + 'news/news?';
  private _getNewsById= this._baseUrl + 'news/news/';
private _myCourses = this._baseUrl + 'courses/course/mycourse/'
private _assessment = this._baseUrl +  'PaidStudent/assessment'
 


  constructor(private http:HttpClient) { }

  // Login Api
  loginUser(users){
    return this.http.post(this._loginUser, users);
  }

  // Course Page Api
  getCourseDetails(){
    return this.http.get<any>(this._getCourseDetails);
  } 

  postCourse(requestdata) {
    return this.http.post(this._postCourse, requestdata);
  }

  posttopic(courseId, topicdata){
    return this.http.post(this._posttopic + courseId + '/topic',topicdata);
  } 

  postContent(contentId, topicId, requestdata){
    return this.http.post(this._getViewFinal300 + contentId +'/'+ topicId + '/contents', requestdata);
  }

  deleteCourse(courseId){
    return this.http.delete<any>(this._getCourseDetails +'/'+ courseId);
  } 

  //view Final Page
  viewFinal360(id){
    return this.http.get(this._getViewFinal300 + id);
  }

  approveCourseAndUpdateCourse(courseId, requestdata){
    return this.http.put(this._approveCourse + courseId + '/approve', requestdata);
  }

  deleteConentByContentId(id){
    return this.http.delete(this._deleteContet + id);
  }

  //topicwisecontent Page
  gettoBeApprove(){
    return this.http.get<any>(this._gettoBeApprove);
  } 

  getfaculty(){
    return this.http.get<any>(this._getfaculty);
  } 

  getmasterCourse(){
    return this.http.get(this._getmasterCourse);
  }

  getmastertopics(courseId){
    return this.http.get(this._getmastertopics+ courseId  + '/topics');
  }

  getMatserContent(topicId){
    return this.http.get(this._getmatsercontent+ topicId  + '/contents');
  }

  putApprove(contentId, requestdata){
    return this.http.put(this._putApprove + contentId + '/approve', requestdata);
  }

  rejectContent(contentId){
    return this.http.put(this._rejectContent + contentId +'/reject', null);
  }

  //Assemble page
  postassemble(data){
    
    return this.http.post(this._postassemble, data);
  }

  getApproveContent(){
    return this.http.get(this._getApproveContent);
  }

  getapprovedcontentbyID(courseId){
    return this.http.get(this._getapprovedcontentbyID+ courseId );
  }

  //Assignment
  getfetchAssignment(){
    return this.http.get(this._getfetchAssignment);
  }

  uploadAssignment(requestData){
    return this.http.post(this._uploadAssignment, requestData);
  }

//news
  viewNews(){
    return this.http.get(this._viewNews);
  }

  deleteNews(id){
    return this.http.delete(this._deleteNews + id);
  }

  getNewsById(id){
    return this.http.get(this._editNews+ id);
  }

  editNews(id,requestdata){
    return this.http.put(this._getNewsById+ '/'+ id , requestdata);
  }

  addNews(requestdata){
    return this.http.post(this._addNews, requestdata);
  }

  myCourses(userId){
    return this.http.get(this._myCourses + userId);
  }

  getCourseDetail(id){
    return this.http.get(this._getCourseDetails+ '/'+ id +'/contentDetails');
  }

  addAssessment(requestData){
    return this.http.post(this._assessment, requestData );
  }

  addQuestions(requestData, assessmentId){
    return this.http.post(this._assessment +'/'+ assessmentId + '/questions', requestData );
  }

  addAnswers(requestData, questionId){
    return this.http.post(this._assessment  +'/' + questionId + '/answer', requestData );
  }

  findAllassessmentList(){
    return this.http.get(this._assessment);
  }

  getAssessmentById(id){
    return this.http.get(this._assessment + "/" + id);
  }
}
