var isEmailEq = false;
$(document).ready(function() {

	if (!$("table#inActiveUserTABLE tbody tr").length) {

		$("div.inactiveUserDIV").empty().append('<h3 class="text-center">Data not found</h3>')
	}

	createTable("userListTABLE", "", "", "", "");
	$("input#serachUserData").keyup(function() {
		var searchFieldVal = $(this).val().toLowerCase();
		if (searchFieldVal.length >= 1) {
			$("table#userListTABLE tbody tr").each(function() {
				var customerNameTDText = $(this).find("td.userName").text().toLowerCase();
				if (customerNameTDText.indexOf(searchFieldVal) > -1) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		} else {
			$("table#userListTABLE tbody tr").show();
		}

	});

	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("table td:last-child").html();

	// Append table with add row form on add new button click
	$(".add-new").click(function() {

		$(".add-new").off("click").attr('href', "javascript: void(0);");
		$("a.edit, a.delete").removeAttr("class");

		$("a.edit").attr("class", "edit");
		$("a.delete").attr("class", "delete");

		$(this).attr("disabled", "disabled");
		var index = $("table tbody tr:last-child").index();
		var row = '<tr>' + '<td><input type="text" class="form-control userName></td>'
		row += '<td><input type="text" class="form-control userName" name="name" id="userName"></td><td><input type="text" class="form-control email" name="emailaddress" id="emailaddress"></td><td><input type="text" class="form-control phone" name="phone" id="phone"></td>';

		//row += '<td><select class="form-control role" id="role"><option value="">Please Select </option>';
		//$(roleList).each(function(i, role) {

			//row += "<option value='" + role.roleName + "'>" + role.roleName + "</option>";

		//});
		//row += '</select></td>';
		//row += '<td><input type="number" class="form-control hourlyRate" name="hourlyRate" id="hourlyRate"></td>';
		//row += '<td><select name="hatIds" id="hatIdSELECT" data-placeholder="Assigned  Hats" multiple class="chosen-select form-control" tabindex="8">';

		$(hatList).each(function(i, hat) {
			if (hat.isDeleted != "Y") {
				row += "<option value='" + hat.hatId + "'>" + hat.hatName + "</option>";
			}
		});
		row += "</select></td>";
		row += '<td><select name="dept" id="deptSELECT" class="form-control dept"><option value="">Please Select </option>';

		$(deptList).each(function(i, dept) {
			row += "<option value='" + dept.departmentId + "'>" + dept.departmentName + "</option>";
		});
		row += "</select></td>";
		row += '<td><select name="type" id="userTypeSELECT" class="form-control userTypeSELECT" onclick="userTypeSelect(this);"><option value="Internal">Internal</option><option value="External">External</option>';
		row += '<td>' + actions + '</td>' + '</tr>';

		$('table > tbody > tr:first').before(row);
		$('table > tbody > tr:first').find(".add, .edituser").toggle();
		//$("table").append(row);
		//$("table tbody tr").eq(index + 1).find(".add, .edituser").toggle();
		$('[data-toggle="tooltip"]').tooltip();
		$('.chosen-select').chosen();
	});

	// Add row on add button click
	$(document).on("click", ".add", function() {
		var trObj = $(this).closest('tr');
		var userId = trObj.attr("data-user-id");
		if ($(trObj).length) {
			if ($(trObj).find("td input.userName").val() == "") {
				$(trObj).find("td input.userName").addClass("error");
			} else if ($(trObj).find("td input.hourlyRate").val() == "") {
				$(trObj).find("td input.hourlyRate").addClass("error");
			} else if ($(trObj).find("td input.phone").val() == "") {
				$(trObj).find("td input.phone").addClass("error");
			} else if ($(trObj).find("td input.email").val() == "" || !($(trObj).find("td input.email").val().includes("@"))) {
				$(trObj).find("td input.email").addClass("error");
			}

			else {
				if (userId == undefined) {
					userId = 0;
				}
				var requestData = {
					"inputEmail" : $(trObj).find("td input.email").val(),
					"userId" : userId,
				}

				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "validateEmail",
					dataType : "json",
					data : JSON.stringify(requestData),
					success : function(response) {
						if (response != null && response.message == "Equal" && response.status == false) {

							bootbox.alert("EmailId already exists");

						} else {
							var ids = [];
							if ($("select#hatIdSELECT").val() != null && $("select#hatIdSELECT").val() != '' && userId == 0) {
								ids = $("select#hatIdSELECT").val();
							} else if ($("select#hatIdEditSELECT").val() != null && $("select#hatIdEditSELECT").val() != '' && userId != 0) {
								ids = $("select#hatIdEditSELECT").val();
							}
							var department = "";
							if ($(trObj).find("td select.userTypeSELECT option:selected").val() != undefined && $(trObj).find("td select.userTypeSELECT option:selected").val() == "Internal") {
								department = $(trObj).find("td select.dept option:selected").val();
							} else if ($(trObj).find("td select.userTypeSELECT option:selected").val() == undefined && $(trObj).find("td.type").text() == "Internal") {
								department = $(trObj).find("td select.dept option:selected").val();
							}
							var requestData = {
								"salutation" : $(trObj).find("td select.salutation").val(),
								"userName" : $(trObj).find("td input.userName").val() == undefined ? "NA" : $(trObj).find("td input.userName").val(),
								"email" : $(trObj).find("td input.email").val() == undefined ? "NA" : $(trObj).find("td input.email").val(),
								"phone" : $(trObj).find("td input.phone").val() == undefined ? "NA" : $(trObj).find("td input.phone").val(),
								"role" : $(trObj).find("td select.role").val(),
								"hourlyRate" : $(trObj).find("td input.hourlyRate").val() == undefined ? "NA" : $(trObj).find("td input.hourlyRate").val(),
								"department" : department,
								"type" : $(trObj).find("td select.userTypeSELECT option:selected").val() == undefined ? $(trObj).find("td.type").text() : $(trObj).find("td select.userTypeSELECT option:selected").val(),
								"hatIds" : ids,
							}
							$(this).removeClass("error");
							$(document).off("click").attr('href', "javascript: void(0);");
							$(".edituser").off("click").attr('href', "javascript: void(0);");
							$(".deleteuser").off("click").attr('href', "javascript: void(0);");
							submitEditedUser(requestData, userId);

						}
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},

				});
			}
		}
	});

	// Edit row on edit button click
	$(document).on("click", ".edituser", function() {
		$(this).parents("tr").find("td:not(:last-child)").each(function() {
			var row = $(this).closest('tr');
			var isInternal = true;

			if (row.find('td.type').text() == 'External') {
				isInternal = false;
			}
			var tempClass = $(this).attr("class");

			if (tempClass == "salutation") {
				$(this).html('<select class="form-control salutation" name="salutation"><option value="Mr.">Mr.</option><option value="Mrs.">Mrs.</option><option value="Miss">Miss</option><option value="Dr.">Dr.</option><option value="Er.">Er.</option><option value="Sal">Sal</option><option value="Others">Others</option></select>');
			} else if (tempClass == "role") {
				var rowSelect = '';
				if (isInternal) {
					rowSelect = '<select class="form-control role" id="role"><option value="">Please Select </option>';
					$(roleList).each(function(i, role) {

						rowSelect += "<option value='" + role.roleName + "'>" + role.roleName + "</option>";

					});
					rowSelect += '</select>';

					$(this).html(rowSelect);
					if ($(this).attr('data-user-role') != null && $(this).attr('data-user-role') != "") {
						$(this).find('select#role').val($(this).attr('data-user-role'));
					}
				} else {
					rowSelect = '<select class="form-control role" id="role" disabled><option value="External Agency">External Agency </option></select>';
					$(this).html(rowSelect);
				}

			} else if (tempClass == "email") {
				$(this).html('<input type="text" class="form-control ' + tempClass + '" value="' + $(this).text() + '">');
			} else if (tempClass == "hats") {
				if (isInternal) {
					var hatRow = '';
					hatRow += '<select name="hatIds" id="hatIdEditSELECT" data-placeholder="Assigned  Hats" multiple class="chosen-select-edit form-control" tabindex="8">';

					$(hatList).each(function(i, hat) {
						if (hat.isDeleted != "Y") {
							hatRow += "<option value='" + hat.hatId + "'>" + hat.hatName + "</option>";
						}
					});
					hatRow += "</select>";

					$(this).html(hatRow);
					$('.chosen-select-edit').chosen();
					if ($(this).attr('data-hats-id') != null && $(this).attr('data-hats-id').length > 0) {

						var temparrHar = [];

						var temparr = $(this).attr('data-hats-id');
						temparr = temparr.replace(/\s/g, '')
						temparr = temparr.slice(1, -1);
						temparrHar = temparr.split(',');
						if (temparrHar != "") {
							$(this).find('select.chosen-select-edit').val(temparrHar).trigger("chosen:updated");
						}

					}
				} else {
					$(this).find('select.chosen-select-edit').attr("disabled", true);
				}

			} else if (tempClass == "dept") {
				var row = '';
				if (isInternal) {
					row = '<select id="deptEditSELECT" class="form-control dept"><option value="">Please Select </option>';
					$(deptList).each(function(i, dept) {
						row += "<option value='" + dept.departmentId + "'>" + dept.departmentName + "</option>";
					});
					row += "</select>";
					$(this).html(row);
					if ($(this).attr('data-department-id') != null && $(this).attr('data-department-id') != "") {

						$(this).find('select#deptEditSELECT').val($(this).attr('data-department-id'));

					}
				} else {
					row = '<select id="deptEditSELECT" class="form-control dept" disabled><option value="">Please Select </option></select>';
					$(this).html(row);
				}

			} else if (tempClass == "hourlyRate") {
				$(this).html('<input type="number" class="form-control ' + tempClass + '" value="' + $(this).text() + '">');
			} else if (tempClass == "type") {
				//

			}

			else {
				$(this).html('<input type="text" class="form-control ' + tempClass + '" value="' + $(this).text() + '">');
			}

		});
		$("a.edit, a.delete").removeAttr("class");

		$("a.edit").attr("class", "edit");
		$("a.delete").attr("class", "delete");

		$(this).parents("tr").find(".add, .edituser").toggle();
		$(".add-new").attr("disabled", "disabled");
	});

	// Delete row on delete button click
	$(document).on("click", ".deleteuser", function() {
		var trObj = $(this).closest('tr');
		var userId = trObj.attr("data-user-id");
		deleteUser(userId, this);
	});
});

function submitEditedUser(requestData, userId) {
	if ((!$.isEmptyObject(requestData))) {
		var urlData;
		if (userId == 0 || userId == undefined) {
			urlData = "submitEditedUser";
		} else {
			urlData = "submitEditedUser?userId=" + userId;
		}

		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : urlData,
			dataType : "json",
			data : JSON.stringify(requestData),
			success : function(response) {
				if (response != null && response.message == "Email sent" && response.status == true) {
					bootbox.alert("Password for new user has been emailed", function() {

						location.reload();

					});
				} else if (response != null && response.message == "Equal" && response.status == false) {
					bootbox.alert("Email Id already exists");
				} else {
					location.reload();
				}

			},
			error : function(e) {
				console.log("ERROR: ", e);
			},

		});
	}

}

function deleteUser(userId, context) {
	bootbox.confirm("Are you sure to delete this user?", function(result) {
		if (result) {
			$.ajax({
				type : "GET",
				contentType : "application/json",
				url : "deleteUser?userId=" + userId,
				success : function(response) {
					if (response != null) {
						$(context).parents("tr").remove();
						window.location = 'addUser';
					}
				},
				error : function(e) {
					console.log("ERROR: ", e);
				},
			});
		}
	});

}

function validateEmail(ctx) {

	if ($(ctx).val() != '') {
		var inputEmail = $(ctx).val();
		var requestData = {
			"inputEmail" : inputEmail,
		}
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "validateEmail",
			dataType : "json",
			data : JSON.stringify(requestData),
			success : function(response) {
				if (response != null && response.message == "Equal" && response.status == false) {

					bootbox.alert("EmailId already exists");

				}
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},

		});
	}

}

//To De-activate User
function deactivateUser(ctx) {
	var userId = $(ctx).attr('user-id');
	bootbox.confirm("Are you sure to deactivate this user?", function(result) {
		if (result) {
			$.ajax({
				type : "GET",
				contentType : "application/json",
				dataType : "json",
				url : "deactivateUser?userId=" + userId,
				success : function(response) {
					if (response != null && response.message == "Success" && response.status == true) {

						location.reload();

					}
				},
				error : function(e) {
					console.log("ERROR: ", e);
				},
			});
		}

	});
}

//Activate User
function activateUser(ctx) {
	var userId = $(ctx).attr('user-id');
	bootbox.confirm("Are you sure to activate this user?", function(result) {
		if (result) {
			$.ajax({
				type : "GET",
				contentType : "application/json",
				dataType : "json",
				url : "activateUser?userId=" + userId,
				success : function(response) {
					if (response != null && response.message == "Success" && response.status == true) {

						location.reload();

					}
				},
				error : function(e) {
					console.log("ERROR: ", e);
				},
			});
		}

	});
}

function userTypeSelect(ctx) {
	var type = $(ctx).val();
	var row = $(ctx).closest('tr');
	if (type == "External") {
		row.find('td select.dept').attr('disabled', true);
		row.find('td select#role').val("External Agency");
		row.find('td select.role').attr('disabled', true);
		row.find('select.chosen-select').attr("disabled", true);
		/*row.find('.chosen-select').attr('disabled', true);*/

	} else {
		row.find('td select.dept').removeAttr('disabled');
		row.find('td select#hatIdSELECT').removeAttr('disabled');

	}
}